/*
 * File: app/view/HelpViewWindow.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Code.view.HelpViewWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.helpviewwindow',

    requires: [
        'Code.view.HelpViewWindowViewModel',
        'Ext.tab.Panel',
        'Ext.tab.Tab'
    ],

    viewModel: {
        type: 'codeviewwindow'
    },
    height: 563,
    width: 920,
    layout: 'fit',
    title: '帮助说明',

    items: [
        {
            xtype: 'tabpanel',
            title: '',
            activeTab: 1,
            tabPosition: 'bottom',
            items: [
                {
                    xtype: 'panel',
                    html: '<iframe src="/code/helpview1" width="100%" height="100%"></iframe>',
                    title: '代码模版说明'
                },
                {
                    xtype: 'panel',
                    html: '<iframe src="/code/helpview2" width="100%" height="100%"></iframe>',
                    title: '字段数据说明'
                }
                ,
                {
                    xtype: 'panel',
                    html: '<iframe src="/code/showdir" width="100%" height="100%"></iframe>',
                    title: '目录'
                }
            ]
        }
    ]

});