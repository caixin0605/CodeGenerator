/**
 * 查询匹配方式
 */
Ext.define('Code.store.MatchStore', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.field.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'MatchStore',
            data: [
				{
                    type: 'none',
                    value: '非查询条件'
                },
                {
                    type: 'any',
                    value: '模糊匹配'
                },
                {
                    type: 'eq',
                    value: '精确匹配'
                },
                
                {
                    type: 'select',
                    value: '下拉选择'
                },
                {
                    type: 'left',
                    value: '左匹配'
                },
                {
                    type: 'right',
                    value: '右匹配'
                },
                {
                    type: 'dateRange',
                    value: '日期范围'
                },
                {
                    type: 'numberRange',
                    value: '数量范围'
                }
            ],
            fields: [
                {
                    name: 'type'
                },
                {
                    name: 'value'
                }
            ]
        }, cfg)]);
    }
});