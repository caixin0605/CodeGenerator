package com.mmk.code.core.service;

import com.mmk.code.core.model.Project;
import com.mmk.code.core.model.ProjectAuthor;
import com.mmk.gene.service.BaseService;

/**
*@Title: ProjectAuthorService
*@Description: 项目 业务服务层接口
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:56
*/
public interface ProjectAuthorService extends BaseService<ProjectAuthor, Long> {
    
    /**
     * 根据给定的字段和属性值，获得符合条件的第一个结果
     * @param field Project 中的某个字段
     * @return Project 返回符合条件的结果，如果没有返回null
     * @author code generator
     * @date 2016-07-19 14:16:56
     * 
     */
    ProjectAuthor findBy(String field,Object value);
    
    ProjectAuthor findByAuthorAndProjectId(String author,Object projectId);
}