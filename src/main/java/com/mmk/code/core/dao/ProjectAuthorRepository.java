package com.mmk.code.core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mmk.code.core.model.ProjectAuthor;

/**
*@Title: ProjectAuthorRepository
*@Description: 项目 数据资源层
*@author code generator
*@version 1.0
*@date 2016-07-19 14:16:56
*/
public interface ProjectAuthorRepository extends JpaRepository<ProjectAuthor, Long>{


}